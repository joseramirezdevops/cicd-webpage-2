from flask import Flask
from flask_frozen import Freezer

#1. Instantiate a class
app = Flask(__name__)
freezer = Freezer(app)

# 2. Define Paths for Web pages
app.config['FREEZER_BASE_URL'] = 'https://joseramirezdevops.gitlab.io' # Set up username URL
                                  
app.config['FREEZER_DESTINATION'] = 'public' # Set folder to paste in information from website

# 3. Define Custom function
@app.cli.command("renderCustomCommand")
def renderCustomCommand():
    freezer.freeze()

# 4. Path Web requests/Responses
@app.route('/')
def displayMesssage():
    return "Hello. Today is Saturday. May 15, 2021 nuevo "

